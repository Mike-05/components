<?php defined('B_PROLOG_INCLUDED') || die;

$MESS['DODO_DEAL_LIST_HEADER_ID'] = 'ID';
$MESS['DODO_DEAL_LIST_HEADER_TITLE'] = 'Заголовок';
$MESS['DODO_DEAL_LIST_GRID_ROW_COUNT'] = 'Всего: #COUNT#';
$MESS['DODO_DEAL_LIST_HEADER_CREATED_BY_ID'] = 'Кем создан';
$MESS['DODO_DEAL_LIST_HEADER_DATE_CREATE'] = 'Дата создания';
$MESS['DODO_DEAL_LIST_FILTER_PRESET_REQUESTS'] = 'Мои Сделки';
$MESS['DODO_DEAL_LIST_GROUP_ID_EMPTY'] = 'Не выберите направление сделки пожалуйста';