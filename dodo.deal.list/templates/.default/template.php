<?php defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Loader;
use Bitrix\Main\Web\Json;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Grid\Panel\Snippet;

/** @var CBitrixComponentTemplate $this */
/** @var CBitrixComponentTemplate $arResult */
/** @var CBitrixComponentTemplate $arParams */
/** @var CBitrixComponentTemplate $APPLICATION */

if (!Loader::includeModule('crm')) {
    ShowError(Loc::getMessage('DODO_DEAL_LIST_NO_MODULE'));
    return;
}

if ($arParams['PAGE_TITLE']) {
    $APPLICATION->SetTitle($arParams['PAGE_TITLE']);
}

$asset = Asset::getInstance();
$asset->addJs('/bitrix/js/crm/interface_grid.js');

$gridManagerId = $arResult['GRID_ID'] . '_MANAGER';
$rows = array();

foreach ($arResult['DODO_DEALS'] as $deal) {

    $viewUrl = '/crm/deal/details/'.$deal['ID'].'/';

    $deleteUrlParams = http_build_query(array(
        'action_button_' . $arResult['GRID_ID'] => 'delete',
        'ID' => array($deal['ID']),
        'sessid' => bitrix_sessid()
    ));

    $deleteUrl = $arParams['SEF_FOLDER'] . '?' . $deleteUrlParams;

    $rows[] = array(
        'id' => $deal['ID'],
        'actions' => array(
            array(
                'TITLE' => Loc::getMessage('DODO_DEAL_LIST_ACTION_VIEW_TITLE'),
                'TEXT' => Loc::getMessage('DODO_DEAL_LIST_ACTION_VIEW_TEXT'),
                'ONCLICK' => 'BX.Crm.Page.open(' . Json::encode($viewUrl) . ')',
                'DEFAULT' => true
            ),
            array(
                'TITLE' => Loc::getMessage('DODO_DEAL_LIST_ACTION_DELETE_TITLE'),
                'TEXT' => Loc::getMessage('DODO_DEAL_LIST_ACTION_DELETE_TEXT'),
                'ONCLICK' => 'BX.CrmUIGridExtension.processMenuCommand(' . Json::encode($gridManagerId) . ', BX.CrmUIGridMenuCommand.remove, { pathToRemove: ' . Json::encode($deleteUrl) . ' })',
            ),
        ),
        'data' => $deal,
        'columns' => array(
            'ID' => $deal['ID'],
            'NAME' => '<a href="' . $viewUrl . '" target="_self">' . $deal['TITLE'] . '</a>',
            'CREATED_BY_ID' => empty($deal['CREATED_BY_ID']) ? '' : CCrmViewHelper::PrepareUserBaloonHtml(
                array(
                    'PREFIX' => "DEAL_{$deal['ID']}_CREATED",
                    'USER_ID' => $deal['CREATED_BY_ID'],
                    'USER_NAME'=> $deal['FULL_NAME'],
                    'USER_PROFILE_URL' => Option::get('intranet', 'path_user', '', SITE_ID) . '/'
                )
            ),
        )
    );
}

$snippet = new Snippet();

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.grid',
    'titleflex',
    array(
        'GRID_ID' => $arResult['GRID_ID'],
        'HEADERS' => $arResult['HEADERS'],
        'ROWS' => $rows,
        'PAGINATION' => $arResult['PAGINATION'],
        'SORT' => $arResult['SORT'],
        'FILTER' => $arResult['FILTER'],
        'FILTER_PRESETS' => $arResult['FILTER_PRESETS'],
        'IS_EXTERNAL_FILTER' => false,
        'ENABLE_LIVE_SEARCH' => $arResult['ENABLE_LIVE_SEARCH'],
        'DISABLE_SEARCH' => $arResult['DISABLE_SEARCH'],
        'ENABLE_ROW_COUNT_LOADER' => true,
        'AJAX_ID' => '',
        'AJAX_OPTION_JUMP' => 'N',
        'AJAX_OPTION_HISTORY' => 'N',
        'AJAX_LOADER' => null,
        'ACTION_PANEL' => array(
            'GROUPS' => array(
                array(
                    'ITEMS' => array(
                        $snippet->getRemoveButton(),
                        $snippet->getForAllCheckbox(),
                    )
                )
            )
        ),
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y',)
);
