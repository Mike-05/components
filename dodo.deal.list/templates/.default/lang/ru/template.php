<?php defined('B_PROLOG_INCLUDED') || die;

$MESS['CRM_ALL'] = 'Всего';
$MESS['DODO_DEAL_LIST_TITLE'] = 'Сделки';
$MESS['CRM_SHOW_ROW_COUNT'] = 'Показать количество';
$MESS['DODO_DEAL_LIST_ACTION_DELETE_TEXT'] = 'Удалить';
$MESS['DODO_DEAL_LIST_DELETE_DIALOG_BUTTON'] = 'Удалить';
$MESS['DODO_DEAL_LIST_ACTION_VIEW_TEXT'] = 'Просмотреть';
$MESS['DODO_DEAL_LIST_ACTION_DELETE_TITLE'] = 'Удалить сделку';
$MESS['DODO_DEAL_LIST_DELETE_DIALOG_TITLE'] = 'Удалить сделку';
$MESS['DODO_DEAL_LIST_NO_MODULE'] = 'Модуль CRM не установлен.';
$MESS['DODO_DEAL_LIST_ACTION_VIEW_TITLE'] = 'Просмотреть сделку';
$MESS['DODO_DEAL_LIST_DELETE_DIALOG_MESSAGE'] = 'Вы уверены, что хотите удалить выбранную сделку?';
