<?php defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;
use Bitrix\Crm\Category\DealCategory;

/** @var CMain $APPLICATION */
$arDealCategory = array();

// Получаем все категории сделок
$dbDealCategories = DealCategory::getList([
    'select' => ['*'],
    'filter' => ['IS_LOCKED' => 'N']
]);

while ($dealCategory = $dbDealCategories->Fetch()) {
    $arDealCategory[$dealCategory["ID"]] = "[".$dealCategory["ID"]."] ".$dealCategory["NAME"];
}

$arComponentParameters = array(
    'PARAMETERS' => array(
        "CATEGORY_ID" => array(
            "PARENT" => "SETTINGS",
            "NAME" => Loc::getMessage("DODO_DEAL_CATEGORY"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $arDealCategory,
            "REFRESH" => "Y"
        ),
        "PAGE_TITLE" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("DODO_DEAL_TITLE_NAME"),
            "TYPE" => "STRING",
            "REFRESH" => "Y"
        ),
    ),
);
