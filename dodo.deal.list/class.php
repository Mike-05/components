<?php defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Grid;
use Bitrix\Main\Web\Uri;
use Bitrix\Main\Context;
use Bitrix\Crm\DealTable;
use Bitrix\Main\Web\Json;
use Bitrix\Main\UI\Filter;
use Bitrix\Main\SystemException;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Main\ObjectPropertyException;

class dodoDealList extends CBitrixComponent
{
    const GRID_ID = 'DODO_DEAL_LIST';
    const SORTABLE_FIELDS = array('ID', 'TITLE', 'DATE_CREATE', 'CREATED_BY_ID');
    const FILTERABLE_FIELDS = array('ID', 'TITLE', 'DATE_CREATE', 'ASSIGNED_BY_ID');
    const SUPPORT_ACTIONS = array('delete');
    const SUPPORT_SERVICE_ACTIONS = array('GET_ROW_COUNT');

    private static array $headers;
    private static array $filterFields;
    private static array $filterPresets;

    public function __construct(CBitrixComponent $component = null)
    {
        global $USER;
        parent::__construct($component);

        self::$headers = array(
            array(
                'id' => 'ID',
                'name' => Loc::getMessage('DODO_DEAL_LIST_HEADER_ID'),
                'sort' => 'ID',
                'first_order' => 'desc',
                'type' => 'int',
            ),
            array(
                'id' => 'TITLE',
                'name' => Loc::getMessage('DODO_DEAL_LIST_HEADER_TITLE'),
                'sort' => 'TITLE',
                'default' => true,
            ),
            array(
                'id' => 'DATE_CREATE',
                'name' => Loc::getMessage('DODO_DEAL_LIST_HEADER_DATE_CREATE'),
                'sort' => 'DATE_CREATE',
                'default' => true,
            ),
            array(
                'id' => 'CREATED_BY_ID',
                'name' => Loc::getMessage('DODO_DEAL_LIST_HEADER_CREATED_BY_ID'),
                'sort' => 'CREATED_BY_ID',
                'default' => true,
            ),
        );

        self::$filterFields = array(
            array(
                'id' => 'ID',
                'name' => Loc::getMessage('DODO_DEAL_LIST_HEADER_ID')
            ),
            array(
                'id' => 'TITLE',
                'name' => Loc::getMessage('DODO_DEAL_LIST_HEADER_TITLE'),
                'default' => true,
            ),
            array(
                'id' => 'ASSIGNED_BY_ID',
                'name' => Loc::getMessage('DODO_DEAL_LIST_HEADER_CREATED_BY_ID'),
                'type' => 'custom_entity',
                'params' => array(
                    'multiple' => 'Y'
                ),
                'selector' => array(
                    'TYPE' => 'user',
                    'DATA' => array(
                        'ID' => 'ASSIGNED_BY_ID',
                        'FIELD_ID' => 'ASSIGNED_BY_ID'
                    )
                ),
                'default' => true,
            ),
        );

        self::$filterPresets = array(
            'my_deals' => array(
                'name' => Loc::getMessage('DODO_DEAL_LIST_FILTER_PRESET_REQUESTS'),
                'fields' => array(
                    'ASSIGNED_BY_ID' => $USER->GetID(),
                    'ASSIGNED_BY_ID_name' => $USER->GetFullName(),
                )
            ),
        );
    }

    /**
     * @throws \Bitrix\Main\LoaderException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function executeComponent(): void
    {
        if (empty($this->arParams['CATEGORY_ID'])) {
            ShowError(Loc::getMessage('DODO_DEAL_LIST_GROUP_ID_EMPTY'));
            return;
        }

        $context = Context::getCurrent();
        $request = $context->getRequest();
        $grid = new Grid\Options(self::GRID_ID);

        //Начало сортировки
        $gridSort = $grid->getSorting();
        //$field - это переменная, которая представляет текущее поле сортировки, обрабатываемое функцией
        // array_filter() в ходе фильтрации массива $gridSort['sort'].
        $sort = array_filter(
            $gridSort['sort'],
            function ($field) {
                return in_array($field, self::SORTABLE_FIELDS);
            },
            ARRAY_FILTER_USE_KEY
        );
        if (empty($sort)) {
            $sort = array('ID' => 'asc');
        }
        //Конец сортировки

        //Начало фильтра
        $gridFilter = new Filter\Options(self::GRID_ID, self::$filterPresets);
        $gridFilterValues = $gridFilter->getFilter(self::$filterFields);

        //$field - это переменная, которая представляет текущее поле сортировки, обрабатываемое функцией
        // array_filter() в ходе фильтрации массива $gridSort['sort'].
        $gridFilterValues = array_filter(
            $gridFilterValues,
            function ($fieldName) {
                return in_array($fieldName, self::FILTERABLE_FIELDS);
            },
            ARRAY_FILTER_USE_KEY
        );
        //Конец фильтра

        $this->processGridActions($gridFilterValues);
        $this->processServiceActions($gridFilterValues);

        //Начало Pagination
        $gridNav = $grid->GetNavParams();
        $pager = new PageNavigation('');
        $pager->setPageSize($gridNav['nPageSize']);

        if (!array_key_exists('CATEGORY_ID', $gridFilterValues)) {
            $gridFilterValues['CATEGORY_ID'] = $this->arParams['CATEGORY_ID'];
        }

        $pager->setRecordCount(DealTable::getCount($gridFilterValues));

        if ($request->offsetExists('page')) {
            $currentPage = $request->get('page');
            $pager->setCurrentPage($currentPage > 0 ? $currentPage : $pager->getPageCount());
        } else {
            $pager->setCurrentPage(1);
        }
        //Начало Pagination

        $supportRequests = $this->getDeals(array(
            'filter' => $gridFilterValues,
            'limit' => $pager->getLimit(),
            'offset' => $pager->getOffset(),
            'order' => $sort
        ));

        $requestUri = new Uri($request->getRequestedPage());
        $requestUri->addParams(array('sessid' => bitrix_sessid()));

        // Передаем данные в template
        $this->arResult = array(
            'GRID_ID' => self::GRID_ID,
            'DODO_DEALS' => $supportRequests,
            'HEADERS' => self::$headers,
            'PAGINATION' => array(
                'PAGE_NUM' => $pager->getCurrentPage(),
                'ENABLE_NEXT_PAGE' => $pager->getCurrentPage() < $pager->getPageCount(),
                'URL' => $request->getRequestedPage(),
            ),
            'SORT' => $sort,
            'FILTER' => self::$filterFields,
            'FILTER_PRESETS' => self::$filterPresets,
            'ENABLE_LIVE_SEARCH' => false,
            'DISABLE_SEARCH' => true,
            'SERVICE_URL' => $requestUri->getUri(),
        );

        $this->includeComponentTemplate();
    }

    /**
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    // Получаем сделки
    private function getDeals($params = array()): array
    {
        $dbDeals = DealTable::getList($params);
        $arDeals = $dbDeals->fetchAll();

        foreach ($arDeals as &$deal) {
            if ($deal['CREATED_BY_ID']) {
                $deal['FULL_NAME'] = $this->getFullName($deal['CREATED_BY_ID']);
            }

            if ($deal['TITLE']) {
                $deal['TITLE'] = '<a target="_self" href="/crm/deal/details/'.$deal['ID'].'/">' . $deal['TITLE'] . '</a>';
            }
        }

        return $arDeals;
    }

    /**
     * @throws ObjectPropertyException
     * @throws SystemException
     * @throws ArgumentException
     * @throws Exception
     */

    private function processGridActions($currentFilter): void
    {
        if (!check_bitrix_sessid()) {return;}
        $context = Context::getCurrent();
        $request = $context->getRequest();
        $action = $request->get('action_button_' . self::GRID_ID);

        if (!in_array($action, self::SUPPORT_ACTIONS)) {
            return;
        }

        $allRows = $request->get('action_all_rows_' . self::GRID_ID) == 'Y';
        if ($allRows) {
            $dbDeals = DealTable::getList(array(
                'filter' => $currentFilter,
                'select' => array('ID'),
            ));

            $dealIds = array();
            foreach ($dbDeals as $deal) {
                $dealIds[] = $deal['ID'];
            }
        } else {
            $dealIds = $request->get('ID');
            if (!is_array($dealIds)) {
                $dealIds = array();
            }
        }

        if (empty($dealIds)) {
            return;
        }

        switch ($action) {
            case 'delete':
                foreach ($dealIds as $dealID) {
                    DealTable::delete($dealID);
                }
                break;
            default:
                break;
        }
    }

    /**
     * @throws ObjectPropertyException
     * @throws SystemException
     * @throws ArgumentException
     */
    private function processServiceActions($currentFilter): void
    {
        global $APPLICATION;
        if (!check_bitrix_sessid()) {
            return;
        }

        $context = Context::getCurrent();
        $request = $context->getRequest();
        $params = $request->get('PARAMS');

        if (empty($params['GRID_ID']) || $params['GRID_ID'] != self::GRID_ID) {
            return;
        }

        $action = $request->get('ACTION');
        if (!in_array($action, self::SUPPORT_SERVICE_ACTIONS)) {
            return;
        }

        $APPLICATION->RestartBuffer();
        header('Content-Type: application/json');

        switch ($action) {
            case 'GET_ROW_COUNT':
                $count = DealTable::getCount($currentFilter);
                echo Json::encode(array(
                    'DATA' => array(
                        'TEXT' => Loc::getMessage('DODO_DEAL_LIST_GRID_ROW_COUNT', array('#COUNT#' => $count))
                    )
                ));
                break;
            default:
                break;
        }
        die;
    }

    // Получаем имя пользователя
    private function getFullName($userID): string|false
    {
        $userData = \CUser::GetByID($userID)->Fetch();
        if ($userData) {
            return $userData['NAME'] .' '. $userData['LAST_NAME'];
        }
        return false;
    }
}