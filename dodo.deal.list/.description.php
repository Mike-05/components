<?php defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;

$arComponentDescription = array(
	'NAME' => Loc::getMessage('DODO_DEAL_LIST_NAME'),
	'DESCRIPTION' => Loc::getMessage('DODO_DEAL_LIST_DESCRIPTION'),
	'SORT' => 20,
	'PATH' => array(
		'ID' => 'crm',
		'NAME' => Loc::getMessage('DODO_CRM_NAME'),
		'CHILD' => array(
			'ID' => 'deal',
			'NAME' => Loc::getMessage('DODO_DEAL_NAME')
		)
	)
);