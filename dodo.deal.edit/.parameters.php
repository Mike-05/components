<?php defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Socialnetwork\WorkgroupTable;

Loader::includeModule("socialnetwork");

/** @var CMain $APPLICATION */
$arGroups = array();
$rsGroups = WorkgroupTable::getList([
    'filter' => ["ACTIVE" => "Y"],
    'order' => ['ID' => 'ASC'],
    'select' => ['ID', 'NAME']
]);

while ($arGroup = $rsGroups->fetch()) {
    $arGroups[$arGroup["ID"]] = "[".$arGroup["ID"]."] ".$arGroup["NAME"];
}

$arComponentParameters = array(
    'PARAMETERS' => array(
        "GROUP_ID" => array(
            "PARENT" => "SETTINGS",
            "NAME" => Loc::getMessage("DODO_DEAL_GROUP"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $arGroups,
            "REFRESH" => "Y"
        ),
        "PAGE_TITLE" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("DODO_DEAL_TITLE_NAME"),
            "TYPE" => "STRING",
            "REFRESH" => "Y"
        ),
    ),
);
