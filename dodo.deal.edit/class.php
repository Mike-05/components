<?php defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Error;
use Bitrix\Main\Loader;
use Bitrix\Main\Context;
use Bitrix\Crm\DealTable;
use Bitrix\Main\Type\Date;
use Bitrix\Main\UserTable;
use Bitrix\Crm\ContactTable;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Localization\Loc;
use Bitrix\Crm\Category\DealCategory;

class dodoDealCreate extends CBitrixComponent
{

    const FORM_ID = 'DODO_DEAL_EDIT';
    const GRID_ID = 'DODO_DEAL_CREATE';
    private $errors;

    /**
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function executeComponent(): void
    {

        global $APPLICATION, $USER;
        $APPLICATION->SetTitle(Loc::getMessage('DODO_DEAL_EDIT_DEFAULT_TITLE'));

        if (!Loader::includeModule('crm')) {
            ShowError(Loc::getMessage('CRM_NO_MODULE'));
            return;
        }

        if (empty($this->arParams['GROUP_ID'])) {
            ShowError(Loc::getMessage('DODO_DEAL_EDIT_GROUP'));
            return;
        }

        $deal = array(
            'TITLE' => '',
            'ASSIGNED_BY_ID' => 0,
            'CATEGORY_ID' => '',
            'CONTACT_ID' => '',
            'SITE_ID' => 0,
            'MODIFY_BY_ID' => $USER->GetID(),
        );

        if (self::isFormSubmitted()) {
            $savedStoreId = $this->processSave($deal);
            if ($savedStoreId > 0) {
                LocalRedirect('/crm/deal/details/'. $savedStoreId .'/');
            }
        }

        $this->arResult =array(
            'FORM_ID' => self::FORM_ID,
            'GRID_ID' => self::GRID_ID,
            'IS_NEW' => empty($deal['ID']),
            'TITLE' => Loc::getMessage('DODO_DEAL_EDIT_DEFAULT_TITLE'),
            'DEALS' => $deal,
            'CATEGORIES' => self::getCategories(),
            'CONTACTS' => self::getContacts(),
            'ERRORS' => $this->errors,
        );

        $this->includeComponentTemplate();
    }

    /**
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     * @throws \Bitrix\Main\ArgumentException
     * @throws Exception
     */

    // Сохраняем данные
    private function processSave($initialDeal): false|int|array
    {
        $submittedDeal = $this->getSubmittedDeal();
        $deal = array_merge($initialDeal, $submittedDeal);
        $this->errors = self::validate($deal);

        if (!$this->errors->isEmpty()) {
            return false;
        }

        $result = DealTable::add($deal);
        if (!$result->isSuccess()) {
            $this->errors->add($result->getErrors());
        }

        if ($result->isSuccess()) {
            return $this->saveTask($deal, $result->getId()) ? $result->getId() : false;
        }

        return false;
    }

    // Получаем данные и проверяем
    private function getSubmittedDeal(): array
    {
        $context = Context::getCurrent();
        $request = $context->getRequest();
        return array(
            'TITLE' => $request->get('NAME'),
            'ASSIGNED_BY_ID' => $request->get('ASSIGNED_BY_ID'),
            'CATEGORY_ID' => $request->get('CATEGORY_LIST'),
            'CONTACT_ID' => $request->get('CONTACT_LIST'),
            'SITE_ID' => SITE_ID,
        );
    }

    /**
     * @throws Exception
     */

    // Создаем и сохраняем задачу
    private function saveTask($params, $dealID): false|int|array
    {
        $arTaskParams = array(
            'TITLE' => $params['TITLE'],
            'RESPONSIBLE_ID' => $params['ASSIGNED_BY_ID'],
            'CREATED_BY' => $params['ASSIGNED_BY_ID'],
            'DEADLINE' => (new Date())->add('7 day'),
            'SITE_ID' => $params['SITE_ID'],
            'UF_CRM_TASK' => 'D_'.$dealID,
            'GROUP_ID' => $this->arParams['GROUP_ID'],
        );

        $taskResult = \CTaskItem::add($arTaskParams, $params['MODIFY_BY_ID']);
        if ($taskResult->getId()) {
            return $taskResult->getId();
        }
        return false;
    }

    /**
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     * @throws \Bitrix\Main\ArgumentException
     */

    // Валидацию реализуем
    private static function validate($deal): ErrorCollection
    {
        $errors = new ErrorCollection();

        if (empty($deal['TITLE'])) {
            $errors->setError(new Error(Loc::getMessage('DODO_DEAL_EDIT_EMPTY_NAME')));
        }

        if (empty($deal['ASSIGNED_BY_ID'])) {
            $errors->setError(new Error(Loc::getMessage('DODO_DEAL_EDIT_EMPTY_ASSIGNED_BY_ID')));
        } else {
            $dbUser = UserTable::getById($deal['ASSIGNED_BY_ID']);
            if ($dbUser->getSelectedRowsCount() <= 0) {
                $errors->setError(new Error(Loc::getMessage('DODO_DEAL_EDIT_ERROR_UNKNOWN_ASSIGNED_BY_ID')));
            }
        }

        if (empty($deal['CATEGORY_ID'])) {
            $errors->setError(new Error(Loc::getMessage('DODO_DEAL_EDIT_EMPTY_CATEGORY_ID')));
        }

        if (empty($deal['CONTACT_ID'])) {
            $errors->setError(new Error(Loc::getMessage('DODO_DEAL_EDIT_EMPTY_CONTACT_ID')));
        }

        return $errors;
    }

    // Реализация отправка данных
    private static function isFormSubmitted(): bool
    {
        $context = Context::getCurrent();
        $request = $context->getRequest();
        $saveAndView = $request->get('saveAndView');
        $saveAndAdd = $request->get('saveAndAdd');
        $apply = $request->get('apply');
        return !empty($saveAndView) || !empty($saveAndAdd) || !empty($apply);
    }

    // Получаем направление сделки
    private function getCategories(): array|false
    {
        $arEntityType = array();

        // Получаем все категории сделок
        $dealCategories = DealCategory::getList([
            'select' => ['ID', 'NAME'],
            'filter' => ['IS_LOCKED' => 'N']
        ]);

        while ($dealCategory = $dealCategories->Fetch()) {
            $arEntityType[$dealCategory["ID"]] = "[".$dealCategory["ID"]."] ".$dealCategory["NAME"];
        }

        return $arEntityType ?? false;
    }

    /**
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     * @throws \Bitrix\Main\ArgumentException
     */

    // Получаем группа
    private function getContacts(): false|array
    {
        $arContacts = array();

        // Получаем список контактов
        $dbContacts = ContactTable::getList([
            'select' => ['ID', 'NAME'],
            //'filter' => ['OPENED' => 'Y'] // Выбираю доступно для всех на сяком случае
            'filter' => ['*']
        ]);

        while ($arContact= $dbContacts->Fetch()) {
            $arContacts[$arContact["ID"]] = "[".$arContact["ID"]."] ".$arContact["NAME"];
        }

        return $arContacts ?? false;
    }
}