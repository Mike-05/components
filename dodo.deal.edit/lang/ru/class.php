<?php defined('B_PROLOG_INCLUDED') || die;

$MESS['CRM_NO_MODULE'] = 'Модуль "CRM" не установлен.';
$MESS['DODO_DEAL_EDIT_DEFAULT_TITLE'] = 'Создать новую сделку';
$MESS['DODO_DEAL_EDIT_TITLE'] = 'Сделка №#ID# &mdash; #NAME#';
$MESS['DODO_DEAL_EDIT_GROUP'] = 'Проект не выбран.';
$MESS['DODO_DEAL_EDIT_EMPTY_NAME'] = 'Название новую сделку не задано.';
$MESS['DODO_DEAL_EDIT_EMPTY_ASSIGNED_BY_ID'] = 'Не указан ответственный.';
$MESS['DODO_DEAL_EDIT_EMPTY_CATEGORY_ID'] = 'Не указан направление сделки.';
$MESS['DODO_DEAL_EDIT_EMPTY_CONTACT_ID'] = 'Не указан контакт сделки.';
$MESS['DODO_DEAL_EDIT_ERROR_UNKNOWN_ASSIGNED_BY_ID'] = 'Указанный ответственный сотрудник не существует.';