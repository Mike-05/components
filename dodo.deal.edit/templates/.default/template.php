<?php defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Error;
use Bitrix\Main\Loader;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Localization\Loc;

/** @var CBitrixComponentTemplate $this */
/** @var CBitrixComponentTemplate $arResult */
/** @var CBitrixComponentTemplate $APPLICATION */

if (!Loader::includeModule('crm')) {
    ShowError(Loc::getMessage('DODO_DEAL_CREATE_NO_MODULE'));
    return;
}

/** @var ErrorCollection $errors */
$errors = $arResult['ERRORS'];

foreach ($errors as $error) {
    /** @var Error $error */
    ShowError($error->getMessage());
}

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.form',
    'edit',
    array(
        'GRID_ID' => $arResult['GRID_ID'],
        'FORM_ID' => $arResult['FORM_ID'],
        'ENABLE_TACTILE_INTERFACE' => 'Y',
        'SHOW_SETTINGS' => 'Y',
        'TITLE' => $arResult['TITLE'],
        'TABS' => array(
            array(
                'id' => 'tab_1',
                'name' => Loc::getMessage('DODO_DEAL_CREATE_NAME'),
                'title' => Loc::getMessage('DODO_DEAL_CREATE_TITLE'),
                'display' => false,
                'fields' => array(
                    array(
                        'id' => 'deal_section',
                        'name' => Loc::getMessage('DODO_DEAL_CREATE_SECTION'),
                        'type' => 'section',
                        'isTactile' => true,
                    ),
                    array(
                        'id' => 'NAME',
                        'name' => Loc::getMessage('DODO_DEAL_FIELD_NAME'),
                        'type' => 'text',
                        'value' => '',
                        'isTactile' => true,
                    ),
                    array(
                        'id' => 'ASSIGNED_BY',
                        'name' => Loc::getMessage('DODO_DEAL_FIELD_ASSIGNED_BY'),
                        'type' => 'intranet_user_search',
                        'value' => '',
                        'componentParams' => array(
                            'NAME' => 'crmstores_edit_responsible',
                            'INPUT_NAME' => 'ASSIGNED_BY_ID',
                            'SEARCH_INPUT_NAME' => 'ASSIGNED_BY_NAME',
                            'NAME_TEMPLATE' => CSite::GetNameFormat()
                        ),
                        'isTactile' => true,
                    ),
                    array(
                        'id' => 'CATEGORY_LIST',
                        'name' => Loc::getMessage('DODO_DEAL_FIELD_CATEGORY'),
                        'type' => 'list',
                        'items' => $arResult['CATEGORIES'],
                        'isTactile' => true,
                    ),
                    array(
                        'id' => 'CONTACT_LIST',
                        'name' => Loc::getMessage('DODO_DEAL_FIELD_CONTACT'),
                        'type' => 'list',
                        'items' => $arResult['CONTACTS'],
                        'isTactile' => true,
                    ),

                )
            ),
        ),
        'BUTTONS' => array(
            'back_url' => $arResult['BACK_URL'],
            'standard_buttons' => true,
        ),
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);