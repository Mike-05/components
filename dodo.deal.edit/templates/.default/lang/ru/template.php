<?php defined('B_PROLOG_INCLUDED') || die;

$MESS['DODO_DEAL_CREATE_NO_MODULE'] = 'Модуль CRM не установлен.';
$MESS['DODO_DEAL_CREATE_NAME'] = 'Создать сделку';
$MESS['DODO_DEAL_CREATE_TITLE'] = 'Свойства торговой точки';
$MESS['DODO_DEAL_CREATE_SECTION'] = 'Создать новую сделку';
$MESS['DODO_DEAL_FIELD_ID'] = 'ID';
$MESS['DODO_DEAL_FIELD_NAME'] = 'Название сделки';
$MESS['DODO_DEAL_FIELD_CATEGORY'] = 'Направления сделки';
$MESS['DODO_DEAL_FIELD_CONTACT'] = 'Список контактов';
$MESS['DODO_DEAL_FIELD_ASSIGNED_BY'] = 'Ответственный';